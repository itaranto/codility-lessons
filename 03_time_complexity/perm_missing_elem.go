package main

// An array A consisting of N different integers is given. The array contains integers in the range
// [1..(N + 1)], which means that exactly one element is missing.
//
// Your goal is to find that missing element.
//
// Write a function:
//
//   func permMissingElem(A []int) int
//
// that, given an array A, returns the value of the missing element.
//
// For example, given array A such that:
//   A[0] = 2
//   A[1] = 3
//   A[2] = 1
//   A[3] = 5
//
// the function should return 4, as it is the missing element.
//
// Write an efficient algorithm for the following assumptions:
//
// - N is an integer within the range [0..100,000];
// - the elements of A are all distinct;
// - each element of array A is an integer within the range [1..(N + 1)].

func permMissingElem(a []int) int {
	n := len(a)

	elems := make([]bool, n+1)
	for _, elem := range a {
		elems[elem-1] = true
	}

	for i, ok := range elems {
		if !ok {
			return i + 1
		}
	}

	panic("unreachable code")
}
