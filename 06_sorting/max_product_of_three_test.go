package main

import (
	"testing"
)

func TestMaxProductOfThree(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		want int
	}{
		{
			name: "Example",
			a:    []int{-3, 1, 2, -2, 5, 6},
			want: 60,
		},
		{
			name: "Example2",
			a:    []int{-5, 5, -5, 4},
			want: 125,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := maxProductOfThree(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
