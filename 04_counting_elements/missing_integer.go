package main

import (
	"gitlab.com/itaranto/codility-lessons/internal"
)

// Write a function:
//
//   func missingInteger(A []int) int
//
// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that
// does not occur in A.
//
// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
//
// Given A = [1, 2, 3], the function should return 4.
//
// Given A = [−1, −3], the function should return 1.
//
// Write an efficient algorithm for the following assumptions:
//
// - N is an integer within the range [1..100,000];
// - each element of array A is an integer within the range [−1,000,000..1,000,000].

func missingInteger(a []int) int {
	n := len(a)

	elements := internal.Set[int]{}
	for _, elem := range a {
		elements[elem] = internal.Empty{}
	}

	for i := 1; i < n+2; i++ {
		if _, ok := elements[i]; !ok {
			return i
		}
	}

	return 1
}
