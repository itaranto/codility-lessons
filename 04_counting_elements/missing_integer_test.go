package main

import (
	"testing"
)

func TestMissingInteger(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		want int
	}{
		{
			name: "01",
			a:    []int{1, 3, 6, 4, 1, 2},
			want: 5,
		},
		{
			name: "02",
			a:    []int{1, 2, 3},
			want: 4,
		},
		{
			name: "03",
			a:    []int{-1, -3},
			want: 1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := missingInteger(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
