package main

import (
	"testing"
)

var frogRiverOneTests = []struct {
	name string
	x    int
	a    []int
	want int
}{
	{
		name: "01",
		x:    5,
		a:    []int{1, 3, 1, 4, 2, 3, 5, 4},
		want: 6,
	},
	{
		name: "02",
		x:    4,
		a:    []int{1, 4, 3, 1, 2, 1, 3, 3, 4, 4, 2, 1, 2, 4, 2, 2, 4, 4, 4, 1},
		want: 4,
	},
}

func TestFrogRiverOneCuadratic(t *testing.T) {
	for _, test := range frogRiverOneTests {
		t.Run(test.name, func(t *testing.T) {
			got := frogRiverOneCuadratic(test.x, test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestFrogRiverOneLinear(t *testing.T) {
	for _, test := range frogRiverOneTests {
		t.Run(test.name, func(t *testing.T) {
			got := frogRiverOneLinear(test.x, test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
