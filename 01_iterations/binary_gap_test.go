package main

import (
	"testing"
)

func TestBinaryGap(t *testing.T) {
	tests := []struct {
		name string
		n    int
		want int
	}{
		{
			name: "01",
			n:    9,
			want: 2,
		},
		{
			name: "02",
			n:    529,
			want: 4,
		},
		{
			name: "03",
			n:    20,
			want: 1,
		},
		{
			name: "04",
			n:    15,
			want: 0,
		},
		{
			name: "05",
			n:    32,
			want: 0,
		},
		{
			name: "06",
			n:    1041,
			want: 5,
		},
		{
			name: "07",
			n:    561892,
			want: 3,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := binaryGap(test.n)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
