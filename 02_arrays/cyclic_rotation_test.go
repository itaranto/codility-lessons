package main

import (
	"slices"
	"testing"
)

func TestCyclicRotation(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		k    int
		want []int
	}{
		{
			name: "01",
			a:    []int{3, 8, 9, 7, 6},
			k:    3,
			want: []int{9, 7, 6, 3, 8},
		},
		{
			name: "02",
			a:    []int{0, 0, 0},
			k:    1,
			want: []int{0, 0, 0},
		},
		{
			name: "03",
			a:    []int{1, 2, 3, 4},
			k:    4,
			want: []int{1, 2, 3, 4},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := cyclicRotation(test.a, test.k)
			if !slices.Equal(got, test.want) {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
