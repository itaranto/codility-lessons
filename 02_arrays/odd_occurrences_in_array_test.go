package main

import (
	"testing"
)

func TestOddOccurrencesInArray(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		want int
	}{
		{
			name: "01",
			a:    []int{9, 3, 9, 3, 9, 7, 9},
			want: 7,
		},
		{
			name: "02",
			a:    []int{1},
			want: 1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := oddOccurrencesInArray(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
