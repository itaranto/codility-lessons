package main

import (
	"testing"
)

var passingCarsTests = []struct {
	name string
	a    []int
	want int
}{
	{
		name: "AllZeroes",
		a:    []int{0, 0, 0, 0, 0},
		want: 0,
	},
	{
		name: "AllOnes",
		a:    []int{1, 1, 1, 1, 1},
		want: 0,
	},
	{
		name: "Mixed",
		a:    []int{0, 1, 0, 1, 1},
		want: 5,
	},
}

func TestPassingCarsCuadratic(t *testing.T) {
	for _, test := range passingCarsTests {
		t.Run(test.name, func(t *testing.T) {
			got := passingCarsCuadratic(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestPassingCarsLinear(t *testing.T) {
	for _, test := range passingCarsTests {
		t.Run(test.name, func(t *testing.T) {
			got := passingCarsLinear(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestPassingCarsLinearBetter(t *testing.T) {
	for _, test := range passingCarsTests {
		t.Run(test.name, func(t *testing.T) {
			got := passingCarsLinearBetter(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
