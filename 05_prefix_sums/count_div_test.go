package main

import (
	"testing"
)

var countDivTests = []struct {
	name string
	a    int
	b    int
	k    int
	want int
}{
	{
		name: "Example",
		a:    6,
		b:    11,
		k:    2,
		want: 3,
	},
	{
		name: "Simple",
		a:    11,
		b:    345,
		k:    17,
		want: 20,
	},
	{
		name: "Minimal1",
		a:    1,
		b:    1,
		k:    11,
		want: 0,
	},
	{
		name: "Minimal2",
		a:    0,
		b:    0,
		k:    11,
		want: 1,
	},
	{
		name: "Extreme1",
		a:    10,
		b:    10,
		k:    5,
		want: 1,
	},
	{
		name: "Extreme2",
		a:    10,
		b:    10,
		k:    7,
		want: 0,
	},
	{
		name: "Extreme3",
		a:    10,
		b:    10,
		k:    20,
		want: 0,
	},
}

func TestCountDivNaive(t *testing.T) {
	for _, test := range countDivTests {
		t.Run(test.name, func(t *testing.T) {
			got := countDivNaive(test.a, test.b, test.k)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestCountDivNaiveBetter(t *testing.T) {
	for _, test := range countDivTests {
		t.Run(test.name, func(t *testing.T) {
			got := countDivNaiveBetter(test.a, test.b, test.k)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestCountDivConstant(t *testing.T) {
	for _, test := range countDivTests {
		t.Run(test.name, func(t *testing.T) {
			got := countDivConstant(test.a, test.b, test.k)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
