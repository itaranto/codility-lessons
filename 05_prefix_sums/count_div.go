package main

// Write a function:
//
//   func countDiv(A int, B int, K int) int
//
// that, given three integers A, B and K, returns the number of integers within the range [A..B]
// that are divisible by K, i.e.:
//
//  { i : A ≤ i ≤ B, i mod K = 0 }
//
// For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three
// numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.
//
// Write an efficient algorithm for the following assumptions:
//
// - A and B are integers within the range [0..2,000,000,000];
// - K is an integer within the range [1..2,000,000,000];
// - A ≤ B.

func countDivNaive(a, b, k int) int {
	divs := 0

	for i := a; i <= b; i++ {
		if i%k == 0 {
			divs++
		}
	}

	return divs
}

func countDivNaiveBetter(a, b, k int) int {
	divs := 0

	i := a
	for i <= b {
		if i%k == 0 {
			divs++
			i += k
		} else {
			i++
		}
	}

	return divs
}

func countDivConstant(a, b, k int) int {
	// Let [a, b] be an interval of positive integers such that 0 <= a <= b, k be the divisor.
	//
	// It's easy to see there's:
	// n(a) = floor(a / k) factors of k in [0, a] and
	// n(b) = floor(b / k) factors of k in [0, b]
	//
	// 0------------a-------------------b---->
	// 0k---1k---2k---3k---4k---5k---6k---7k->
	//
	// Then n(b) - n(a) equals to the number of integers divisible by k in range (a, b].
	// The point a is not included, because the subtracted n(a) includes this point.
	//
	// Therefore, the result should be incremented by one, if a mod k is zero:
	//
	count := (b/k - a/k)

	if a%k == 0 {
		return count + 1
	} else {
		return count
	}
}
